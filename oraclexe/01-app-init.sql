--**********************************
--Ajuste do NLS_CHARACTERSET
--**********************************

connect sys/oracle as sysdba;
shutdown;
startup restrict;
Alter database character set INTERNAL_USE WE8ISO8859P1;
shutdown immediate;
startup;
connect system/oracle

--**********************************
--Tuning OracleXE
--**********************************

alter system set filesystemio_options=directio scope=spfile;
alter system set disk_asynch_io=false scope=spfile;

--**********************************
--Esquema labsuser
--**********************************

create tablespace labsuser datafile '/u01/app/oracle/oradata/XE/labsuser01.dbf' size 100M online;
create tablespace idx_labsuser datafile '/u01/app/oracle/oradata/XE/idx_labsuser01.dbf' size 100M;
create user labsuser identified by labsuser default tablespace labsuser temporary tablespace temp;
grant resource to labsuser;
grant connect to labsuser;
grant create view to labsuser;
grant create procedure to labsuser;
grant create materialized view to labsuser;
alter user labsuser default role connect, resource;

--**********************************
--Disabling 8080-http port
--**********************************

Exec dbms_xdb.sethttpport(0);

exit;