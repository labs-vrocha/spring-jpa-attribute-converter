package com.labs.springjpaattributeconverter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringJpaAttributeConverterApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringJpaAttributeConverterApplication.class, args);
	}

}
